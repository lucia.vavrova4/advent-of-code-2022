from functions import input_file

sections = [[int(char) for char in line.replace('-', ',').split(',')] for line in input_file('../input/04_input.txt')]

print(sections)
result1 = 0
result2 = 0

for item in sections:
    if (item[0] <= item[2] and item[1] >= item[3]) or (item[2] <= item[0] and item[3] >= item[1]):
        result1 += 1

print(f'4a result is: {result1}')

for item in sections:
    if item[0] in range(item[2], item[3] + 1) or \
       item[1] in range(item[2], item[3] + 1) or \
       item[2] in range(item[0], item[1] + 1) or \
       item[3] in range(item[0], item[1] + 1):
        result2 += 1

print(f'4b result is: {result2}')




