from functions import input_file

file = input_file('../input/14_input.txt')
tmp = []
coordinates = []

for line in file:
    for item in line.split(' -> '):
        x = [int(x) for x in item.split(',')]
        x[0] = x[0] - 300
        tmp.append(x)
    coordinates.append(tmp)
    tmp = []

lowest_x = 500
highest_x = 0
lowest_y = 500
highest_y = 0

for line in coordinates:
    for item in line:
        if item[0] < lowest_x:
            lowest_x = item[0]

        if item[0] > highest_x:
            highest_x = item[0]

        if item[1] < lowest_y:
            lowest_y = item[1]

        if item[1] > highest_y:
            highest_y = item[1]


cave = [['.' for x in range(highest_x + 300)] for y in range(highest_y + 2)]

for line in coordinates:

    length = len(line) - 1
    i = 0

    while i < length:
        cave[line[i][1]][line[i][0]] = '#'

        if line[i][1] == line[i + 1][1]:
            for x in range(min(line[i][0], line[i + 1][0]), max(line[i][0], line[i + 1][0]) + 1):
                cave[line[i][1]][x] = '#'

        elif line[i][0] == line[i + 1][0]:
            for y in range(min(line[i][1], line[i + 1][1]), max(line[i][1], line[i + 1][1]) + 1):
                cave[y][line[i][0]] = '#'

        i += 1

x = 0
height = len(cave)
sand = []
count = 0
pos = 200

while x < height - 1:
    x = 0
    pos = 200

    while True:
        if x == height - 1:
            break
        if cave[x + 1][pos] == '.':
            x += 1

        elif cave[x + 1][pos - 1] == '.':
            x += 1
            pos -= 1

        elif cave[x + 1][pos + 1] == '.':
            x += 1
            pos += 1

        else:
            cave[x][pos] = 'o'
            count += 1
            break

print(count)

# 2nd part

tmp = ['#' for _ in range(highest_x + 300)]
cave.append(tmp)

while cave[0][pos] == '.':
    x = 0
    pos = 200

    while True:
        if cave[x][pos] == 'o':
            break
        if cave[x + 1][pos] == '.':
            x += 1

        elif cave[x + 1][pos - 1] == '.':
            x += 1
            pos -= 1

        elif cave[x + 1][pos + 1] == '.':
            x += 1
            pos += 1

        else:
            cave[x][pos] = 'o'
            count += 1
            break

print(count)
