from functions import *

file = input_file2('../input/17_input.txt')

lego = [[(0, 2), (0, 3), (0, 4), (0, 5)],
        [(0, 3), (1, 2), (1, 3), (1, 4), (2, 3)],
        [(0, 4), (1, 4), (0, 2), (0, 3), (2, 4)],
        [(0, 2), (1, 2), (2, 2), (3, 2)],
        [(0, 2), (0, 3), (1, 2), (1, 3)]
        ]

board = [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6)]

c = -1
sides = -1
highest = 0

for _ in range(2022):
    c += 1
    if c == 5:
        c = 0

    current_pos = place_new(lego[c], highest)

    while True:
        sides += 1

        if sides == 10091:
            sides = 0

        if file[sides] == '<':
            new_pos = left(current_pos)
        else:
            new_pos = right(current_pos)

        if is_inside(new_pos) and not_contained(new_pos, board):
            current_pos = [i for i in new_pos]
            new_pos = down(current_pos)

            if not_contained(new_pos, board):
                current_pos = [i for i in new_pos]

            else:
                break

        else:
            new_pos = down(current_pos)

            if not_contained(new_pos, board):
                current_pos = [i for i in new_pos]

            else:
                break

    for item in current_pos:

        if highest < item[0]:
            highest = item[0]

        board.insert(0, item)

print(highest)

i = 0
nums = []
for num in range(highest + 1):
    if (num, i) in board and (num, i+1) in board and (num, i+2) in board and (num, i+3) in board and (num, i+4) in board and (num, i+5) in board and (num, i+6) in board:
        nums.insert(0, num)

check_list = []
for i in range(len(nums) - 1):
    check_list.append([nums[i], nums[i + 1], nums[i] - nums[i + 1]])


print(f'HIGHEST: {highest}')
rounds = 1000000000000

print((rounds - 208) / 1740)

# 574712643

result = 574712643 * 2759 + 1878
print(result)

