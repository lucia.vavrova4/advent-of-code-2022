from functions import input_file, find_num

monkeys = {}

for line in input_file('../input/21_input.txt'):
    x = line.split(' ')

    if len(x) == 2:
        monkeys[x[0][:-1]] = int(x[1])

    else:
        monkeys[x[0][:-1]] = x[1:]

print(find_num(monkeys, 'root'))

# B

lrnp = find_num(monkeys, 'lrnp')
ptnb = find_num(monkeys, 'ptnb')

while lrnp != ptnb:
    monkeys['humn'] += 1
    lrnp = find_num(monkeys, 'lrnp')
    ptnb = find_num(monkeys, 'ptnb')

print(monkeys.get('humn'))


