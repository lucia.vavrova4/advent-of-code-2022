from functions import *

grove = []
c = -1
directions = ['N', 'S', 'W', 'E']

for line in input_file('../input/23_input.txt'):
    c += 1
    d = -1
    for char in line:
        d += 1
        if char == '#':
            grove.append([c, d])

for turn in range(10):
    new_grove = []
    no_moves = 0

    for elf in grove:

        move = no_move(grove, elf)
        index = 0
        if move is not None:
            no_moves += 1

        while move is None:

            if directions[index] == 'N':
                move = check_north(grove, elf)

            elif directions[index] == 'S':
                move = check_north(grove, elf)

            elif directions[index] == 'W':
                move = check_west(grove, elf)

            elif directions[index] == 'E':
                move = check_north(grove, elf)

            index += 1

            if index == 4:
                move = elf
                break

        new_grove.append(move)

    directions.append(directions.pop(0))
    grove = make_move(grove, new_grove)

    # B part:

    # if len(grove) == no_moves:
    #     print(turn)
    #     break

r = -10000
l = 100000

for num in grove:

    if num[1] > r:
        r = num[1]

    if num[1] < l:
        l = num[1]

up_left = [min(grove)[0], l]
down_right = [max(grove)[0], r]
rectangle = 0

for i in range(up_left[0], down_right[0] + 1):
    for j in range(up_left[1], down_right[1] + 1):
        rectangle += 1

print(rectangle - len(grove))
