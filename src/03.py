from functions import input_file
from string import ascii_letters

letter_dict = dict(zip(list(ascii_letters), list(range(1, 57))))
result = 0

for line in input_file('../input/03_input.txt'):
    x = int(len(line) / 2)
    char = (set(line[:x]).intersection(line[x:]))
    result += letter_dict.get((char.pop()))

print(f'3a result is: {result}')


# part 2

group_list = []
tmp_list = []

for line in input_file('../input/03_input.txt'):
    if len(tmp_list) < 3:
        tmp_list.append(line)
    else:
        group_list.append(tmp_list)
        tmp_list = [line]

group_list.append(tmp_list)

result = 0

for group in group_list:
    x = list(map(set, group))
    char = x[0].intersection(*x)
    result += letter_dict.get((char.pop()))

print(f'3b result is: {result}')
