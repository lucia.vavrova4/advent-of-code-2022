from functions import *

input_list = []
tmp_list = []

for line in input_file('../input/11_input.txt'):
    if line != '':
        tmp_list.append(line)
    else:
        input_list.append(tmp_list)
        tmp_list = []


class Monkey:
    def __init__(self, items, operation, test, throw_to):
        self.items = items
        self.operation = operation
        self.test = test
        self.throw_to = throw_to
        self.number_of_processed = 0
        self.common_num = 9699690

    def add_item(self, item):
        self.items.append(item)

    def process(self):
        i = self.items.pop(0)
        x = self.operation(i)
        if x % self.test == 0:
            return self.throw_to.get(True), x % self.common_num
        else:
            return self.throw_to.get(False), x % self.common_num


monkeys_a = [Monkey([96, 60, 68, 91, 83, 57, 85], lambda x: (x * 2) // 3, 17, {True: 2, False: 5}),
             Monkey([75, 78, 68, 81, 73, 99], lambda x: (x + 3) // 3, 13, {True: 7, False: 4}),
             Monkey([69, 86, 67, 55, 96, 69, 94, 85], lambda x: (x + 6) // 3, 19, {True: 6, False: 5}),
             Monkey([88, 75, 74, 98, 80], lambda x: (x + 5) // 3, 7, {True: 7, False: 1}),
             Monkey([82], lambda x: (x + 8) // 3, 11, {True: 0, False: 2}),
             Monkey([72, 92, 92], lambda x: (x * 5) // 3, 3, {True: 6, False: 3}),
             Monkey([74, 61], lambda x: (x * x) // 3, 2, {True: 3, False: 1}),
             Monkey([76, 86, 83, 55], lambda x: (x + 4) // 3, 5, {True: 4, False: 0})]

for i in range(0, 20):
    for monkey in monkeys_a:
        while len(monkey.items) != 0:
            x = monkey.process()
            monkey.number_of_processed += 1
            monkeys_a[x[0]].add_item(x[1])

result1 = [monkey.number_of_processed for monkey in monkeys_a]
result1.sort()
final_num = result1[-1] * result1[-2]
print(f"11a result is:{final_num}")

# part 2

monkeys_b = [Monkey([96, 60, 68, 91, 83, 57, 85], lambda x: (x * 2), 17, {True: 2, False: 5}),
             Monkey([75, 78, 68, 81, 73, 99], lambda x: (x + 3), 13, {True: 7, False: 4}),
             Monkey([69, 86, 67, 55, 96, 69, 94, 85], lambda x: (x + 6), 19, {True: 6, False: 5}),
             Monkey([88, 75, 74, 98, 80], lambda x: (x + 5), 7, {True: 7, False: 1}),
             Monkey([82], lambda x: (x + 8), 11, {True: 0, False: 2}),
             Monkey([72, 92, 92], lambda x: (x * 5), 3, {True: 6, False: 3}),
             Monkey([74, 61], lambda x: (x * x), 2, {True: 3, False: 1}),
             Monkey([76, 86, 83, 55], lambda x: (x + 4), 5, {True: 4, False: 0})]

for i in range(0, 10000):
    for monkey in monkeys_b:
        while len(monkey.items) != 0:
            x = monkey.process()
            monkey.number_of_processed += 1
            monkeys_b[x[0]].add_item(x[1])

result2 = [monkey.number_of_processed for monkey in monkeys_b]
result2.sort()
final_num = result2[-1] * result2[-2]
print(f"11b result is: {final_num}")
