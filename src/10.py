from functions import input_file

input_list = [line.split(' ') for line in input_file('../input/10_input.txt')]
turn = 1
signal = 1
output = [(1, 1)]
result = 0

for line in input_list:
    for x in line:
        turn += 1
        if x.isalpha():
            output.append((turn, signal))
        else:
            signal += int(x)
            output.append((turn, signal))


for line in output:
    if line[0] == 20:
        result += line[1] * 20

    if line[0] == 60:
        result += line[1] * 60

    if line[0] == 100:
        result += line[1] * 100

    if line[0] == 140:
        result += line[1] * 140

    if line[0] == 180:
        result += line[1] * 180

    if line[0] == 220:
        result += line[1] * 220

print(f"10a result is: {result}")

# B

CRT = ''

for item in output:
    if item[0] <= 40:
        if item[0] - 1 == item[1] or item[0] -1 == item[1] -1 or item[0] - 1 == item[1] + 1:
            CRT += '#'
        else:
            CRT += '.'

    elif 40 < item[0] <= 80:
        if item[0] - 41 == item[1] or item[0] - 41 == item[1] -1 or item[0] - 41 == item[1] + 1:
            CRT += '#'
        else:
            CRT += '.'

    elif 80 < item[0] <= 120:
        if item[0] - 81 == item[1] or item[0] - 81 == item[1] - 1 or item[0] - 81 == item[1] + 1:
            CRT += '#'
        else:
            CRT += '.'

    elif 120 < item[0] <= 160:
        if item[0] - 121 == item[1] or item[0] -121 == item[1] - 1 or item[0] - 121 == item[1] + 1:
            CRT += '#'
        else:
            CRT += '.'

    elif 160 < item[0] <= 200:
        if item[0] - 161 == item[1] or item[0] -161 == item[1] - 1 or item[0] - 161 == item[1] + 1:
            CRT += '#'
        else:
            CRT += '.'

    elif 200 < item[0] <= 240:
        if item[0] - 201 == item[1] or item[0] - 201 == item[1] - 1 or item[0] - 201 == item[1] + 1:
            CRT += '#'
        else:
            CRT += '.'

img = []
print("10b result is:")

for num in range(0, 201, 40):
    print(CRT[num:num + 40])


