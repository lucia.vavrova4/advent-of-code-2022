import copy
import math


def input_file(file_name):
    with open(file_name) as f:
        return f.read().split('\n')


def input_file2(file_name):
    with open(file_name) as f:
        return f.read()


def rps(combinations, file):
    result = 0

    for line in file:
        result += combinations.get(line[0]).get(line[2])

    return result


def run_5a(lst, moves):
    res_list = copy.deepcopy(lst)

    for line in moves:

        for num in range(line[0]):
            res_list[line[2]].append(res_list[line[1]].pop())

    return ''.join([item[-1] for item in res_list])


def run_5b(lst, moves):
    res_list = copy.deepcopy(lst)
    tmp_list = []

    for line in moves:

        for num in range(line[0]):
            tmp_list.insert(0, res_list[line[1]].pop())

        res_list[line[2]] = res_list[line[2]] + tmp_list
        tmp_list = []

    return ''.join([item[-1] for item in res_list])


def run_6(file, num):
    for i in range(0, len(file)):
        x = file[i:i + num]
        if len(set(x)) == len(x):
            return i + num


def scenic_score(file, x, y):
    tree_position = []
    tree = file[x][y]

    # trees on the right

    index = 0
    position = 'x'
    for num in file[x]:

        if index > y:
            if int(num) >= int(tree):
                position = index
                break
        index += 1

    if position != 'x':
        tree_position.append(index - y)
    else:
        tree_position.append(len(file[0]) - y - 1)

    # trees on the left

    index = 0
    position = 'x'

    for num in file[x][:y:]:
        if int(num) >= int(tree):
            position = index

        index += 1

    if position != 'x':
        tree_position.append(y - position)
    else:
        tree_position.append(y)

    # trees on the top

    index = -1
    position = 'x'
    for line in file:
        index += 1

        if index < x:
            if int(line[y]) >= int(tree):
                position = index

    if position != 'x':
        tree_position.append(x - position)
    else:
        tree_position.append(x)

    # trees on the bottom

    position = 'x'
    index = -1
    for line in file:
        index += 1

        if index > x:
            if int(line[y]) >= int(tree):
                position = index
                break

    if position != 'x':
        tree_position.append(position - x)
    else:
        tree_position.append(len(file) - x - 1)

    tree_score = math.prod(tree_position)
    return tree_score


def moves(head, tail):
    if head[0] == tail[0] and head[1] != tail[1]:
        if head[1] - tail[1] == 2:
            tail[1] += 1
            return tail

        elif head[1] - tail[1] == -2:
            tail[1] -= 1
            return tail
        else:
            return tail

    elif head[0] != tail[0] and head[1] == tail[1]:
        if head[0] - tail[0] == 2:
            tail[0] += 1
            return tail

        elif head[0] - tail[0] == -2:
            tail[0] -= 1
            return tail

        else:
            return tail

    elif head[0] != tail[0] and head[1] != tail[1]:
        if head[0] - tail[0] == 1 and head[1] - tail[1] == 2:
            tail[0] += 1
            tail[1] += 1
            return tail

        elif head[0] - tail[0] == 2 and head[1] - tail[1] == 1:
            tail[0] += 1
            tail[1] += 1
            return tail

        elif head[0] - tail[0] == -1 and head[1] - tail[1] == 2:
            tail[0] -= 1
            tail[1] += 1
            return tail

        elif head[0] - tail[0] == -2 and head[1] - tail[1] == 1:
            tail[0] -= 1
            tail[1] += 1
            return tail

        elif head[0] - tail[0] == 1 and head[1] - tail[1] == -2:
            tail[0] += 1
            tail[1] -= 1
            return tail

        elif head[0] - tail[0] == 2 and head[1] - tail[1] == -1:
            tail[0] += 1
            tail[1] -= 1
            return tail

        elif head[0] - tail[0] == -1 and head[1] - tail[1] == -2:
            tail[0] -= 1
            tail[1] -= 1
            return tail

        elif head[0] - tail[0] == -2 and head[1] - tail[1] == -1:
            tail[0] -= 1
            tail[1] -= 1
            return tail

        elif head[0] - tail[0] == -2 and head[1] - tail[1] == -2:
            tail[0] -= 1
            tail[1] -= 1
            return tail

        elif head[0] - tail[0] == 2 and head[1] - tail[1] == 2:
            tail[0] += 1
            tail[1] += 1
            return tail

        elif head[0] - tail[0] == 2 and head[1] - tail[1] == -2:
            tail[0] += 1
            tail[1] -= 1
            return tail

        elif head[0] - tail[0] == -2 and head[1] - tail[1] == 2:
            tail[0] -= 1
            tail[1] += 1
            return tail

        else:
            return tail

    else:
        return tail


def compare_list(left, right):
    for i in range(min(len(left), len(right))):

        if type(left[i]) == list and type(right[i]) == list:
            result = compare_list(left[i], right[i])
            if result != 'equal':
                return result

        elif type(left[i]) != list and type(right[i]) == list:
            result = compare_list([left[i]], right[i])
            if result != 'equal':
                return result

        elif type(left[i]) == list and type(right[i]) != list:
            result = compare_list(left[i], [right[i]])
            if result != 'equal':
                return result

        else:
            if left[i] > right[i]:
                return 'greater'

            elif left[i] < right[i]:
                return 'lower'

    if len(left) > len(right):
        return 'greater'

    elif len(left) < len(right):
        return 'lower'

    else:
        return 'equal'


def compare_list_b(left, right):
    for i in range(min(len(left), len(right))):

        if type(left[i]) == list and type(right[i]) == list:
            result = compare_list_b(left[i], right[i])
            if result != 0:
                return result

        elif type(left[i]) != list and type(right[i]) == list:
            result = compare_list_b([left[i]], right[i])
            if result != 0:
                return result

        elif type(left[i]) == list and type(right[i]) != list:
            result = compare_list_b(left[i], [right[i]])
            if result != 0:
                return result

        else:
            if left[i] > right[i]:
                return 1

            elif left[i] < right[i]:
                return -1

    if len(left) > len(right):
        return 1

    elif len(left) < len(right):
        return -1

    else:
        return 0


def down(shape):
    new_shape = []

    for s in shape:
        new_shape.append((s[0] - 1, s[1]))

    return new_shape


def right(shape):
    new_shape = []

    for s in shape:
        new_shape.append((s[0], s[1] + 1))

    return new_shape


def left(shape):
    new_shape = []

    for s in shape:
        new_shape.append((s[0], s[1] - 1))

    return new_shape


def not_contained(lst, board):
    for i in lst:
        if i in board:
            return False

    return True


def is_inside(lst):
    for i in lst:
        if i[1] == -1 or i[1] == 7:
            return False
    return True


def paint_board(board, current, highest):
    paint = [['.'] * 7 for _ in range(highest + 15)]

    for item in board:
        paint[item[0]][item[1]] = '#'

    for item in current:
        paint[item[0]][item[1]] = '#'

    for line in paint:
        print(line)


def place_new(lego, highest):
    new_shape = []

    for i in lego:
        new_shape.append((i[0] + highest + 4, i[1]))

    return new_shape


def no_move(grove, elf):
    if [elf[0], elf[1] + 1] not in grove and [elf[0], elf[1] - 1] not in grove and [elf[0] + 1, elf[1]] not in grove and [elf[0] - 1, elf[1]] not in grove and [elf[0] + 1, elf[1] + 1] not in grove and [elf[0] + 1, elf[1] - 1] not in grove and [elf[0] - 1, elf[1] + 1] not in grove and [elf[0] - 1, elf[1] - 1] not in grove:
        return elf

    else:
        return None


def check_north(grove, elf):
    if [elf[0] - 1, elf[1]] not in grove and [elf[0] - 1, elf[1] + 1] not in grove and [elf[0] - 1,
                                                                                        elf[1] - 1] not in grove:
        return [elf[0] - 1, elf[1]]

    else:
        return None


def check_south(grove, elf):
    if [elf[0] + 1, elf[1]] not in grove and [elf[0] + 1, elf[1] + 1] not in grove and [elf[0] + 1,
                                                                                        elf[1] - 1] not in grove:
        return [elf[0] + 1, elf[1]]

    else:
        return None


def check_west(grove, elf):
    if [elf[0], elf[1] - 1] not in grove and [elf[0] + 1, elf[1] - 1] not in grove and [elf[0] - 1,
                                                                                        elf[1] - 1] not in grove:
        return [elf[0], elf[1] - 1]

    else:
        return None


def check_east(grove, elf):
    if [elf[0], elf[1] + 1] not in grove and [elf[0] + 1, elf[1] + 1] not in grove and [elf[0] - 1,
                                                                                        elf[1] + 1] not in grove:
        return [elf[0], elf[1] + 1]

    return None


def make_move(grove, new_grove):
    result = []
    duplicates = [x for x in new_grove if new_grove.count(x) > 1]

    for i, j in zip(grove, new_grove):
        if j in duplicates:
            result.append(i)
        else:
            result.append(j)

    return result


def find_num(dic, key):
    x = dic.get(key)

    if isinstance(x, int):
        return x
    else:
        i = find_num(dic, x[0])
        j = find_num(dic, x[2])

        if x[1] == '+':
            return i + j

        elif x[1] == '-':
            return i - j

        elif x[1] == '*':
            return i * j

        else:
            return i / j