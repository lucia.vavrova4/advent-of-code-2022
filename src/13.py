from functions import input_file, compare_list, compare_list_b
from functools import cmp_to_key


file = input_file('../input/13_input.txt')

# 13a

input_list = []
tmp_list = []
result = 0
indices = 0

for line in file:
    if len(line) > 1:
        tmp_list.append(eval(line))
    else:
        input_list.append(tmp_list)
        tmp_list = []
input_list.append(tmp_list)

for line in input_list:
    indices += 1

    if compare_list(line[0], line[1]) == 'lower':
        result += indices

print(result)

# 13b

input_list = []
result = 1

for line in file:
    if line != '':
        input_list.append(eval(line))

sorted_input = sorted(input_list, key=cmp_to_key(compare_list_b))

for i in range(len(sorted_input)):
    if sorted_input[i] == [[2]] or sorted_input[i] == [[6]]:
        result *= (i + 1)

print(result)

