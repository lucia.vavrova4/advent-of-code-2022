from functions import *

file = input_file('../input/08_input.txt')

result = [[0 for i in range(len(file))] for j in range(len(file))]
index1 = 0
index2 = 0

for line in file:
    highest = -1

    for char in line:
        if int(char) > int(highest):
            highest = char
            result[index1][index2] = 1

        index2 += 1

    index1 += 1
    index2 = 0

index1 = 0

for line in file:
    index2 = len(file[0]) - 1
    highest = -1

    for char in line[::-1]:
        if int(char) > int(highest):
            highest = char
            result[index1][index2] = 1

        index2 -= 1
    index1 += 1

#vertical


for index in range(len(file[0])):
    highest = -1
    index1 = -1

    for line in file:
        index1 += 1

        if int(line[index]) > int(highest):
            highest = int(line[index])
            result[index1][index] = 1

file.reverse()

for index in range(len(file[0])):
    highest = -1
    index1 = len(file)

    for line in file:
        index1 -= 1

        if int(line[index]) > int(highest):
            highest = int(line[index])
            result[index1][index] = 1

res1 = 0

for line in result:
    res1 += sum(line)

print(res1)

highest = 0

for x in range(len(file)):

    for y in range(len(file[0])):
        result = scenic_score(file, x, y)
        if result > highest:
            highest = result

print(highest)