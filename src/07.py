from functions import input_file


class Dir:
    def __init__(self, name, size, parent):
        self.name = name
        self.size = size
        self.content = []
        self.parent = parent
        self.is_dir = size is None

    def __str__(self):
        if self.is_dir:
            return 'name: {} size: {} is dir content: {}'.format(self.name, self.size, self.content)
        else:
            return 'name: {} size: {} is file'.format(self.name, self.size)

    def __repr__(self):
        return '\n' + self.__str__()

    def get_size(self):
        global total
        global bigger_than

        if self.size is None:
            self.size = 0
            for item in self.content:
                self.size += item.get_size()

            if self.size <= 100000:
                total += self.size

            if self.size >= 10822529:
                bigger_than.append(self.size)

        return self.size


file_sys = Dir('/', None, None)
current_dir = file_sys

for line in input_file('../input/07_input.txt'):
    if line == '$ ls' or line == '$ cd /':
        continue

    elif line == '$ cd ..':
        current_dir = current_dir.parent

    elif line.startswith('$ cd'):
        x = line.split(' ')[2]

        for child in current_dir.content:
            if child.name == x:
                current_dir = child
                break

    else:
        x = line.split(' ')
        current_dir.content.append(Dir(x[1], None if x[0] == 'dir' else int(x[0]), current_dir))

total = 0
bigger_than = []

current_free = 70000000 - file_sys.get_size()
need_free = 30000000 - current_free

bigger_than.sort()
print(bigger_than.pop(0))
print(total)



