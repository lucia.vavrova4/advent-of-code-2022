from functions import input_file

cubes = [[int(x) for x in item.split(',')] for item in input_file('../input/18_input.txt')]
count = 0

for cube in cubes:
    if [cube[0] + 1, cube[1], cube[2]] not in cubes:
        count += 1
    if [cube[0] - 1, cube[1], cube[2]] not in cubes:
        count += 1
    if [cube[0], cube[1] + 1, cube[2]] not in cubes:
        count += 1
    if [cube[0], cube[1] - 1, cube[2]] not in cubes:
        count += 1
    if [cube[0], cube[1], cube[2] + 1] not in cubes:
        count += 1
    if [cube[0], cube[1], cube[2] - 1] not in cubes:
        count += 1

print(count)