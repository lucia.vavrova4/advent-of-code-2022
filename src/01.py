from functions import input_file

elves = []
x = 0

for line in input_file('../input/01_input.txt'):

    if line != '':
        x += int(line)

    else:
        elves.append(x)
        x = 0

elves.sort(reverse=True)

print(f'1a result is {elves[0]}')
print(f'1b result is {elves[0] + elves[1] + elves[2]}')
