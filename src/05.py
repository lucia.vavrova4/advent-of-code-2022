from functions import input_file, run_5a, run_5b

instructions = [[int(char) for char in line.split(' ') if char.isnumeric()]
                for line in input_file('../input/05_input.txt')]

crates = [[''],
          ['J', 'H', 'G', 'M', 'Z', 'N', 'T', 'F'],
          ['V', 'W', 'J'],
          ['G', 'V', 'L', 'J', 'B', 'T', 'H'],
          ['B', 'P', 'J', 'N', 'C', 'D', 'V', 'L'],
          ['F', 'W', 'S', 'M', 'P', 'R', 'G'],
          ['G', 'H', 'C', 'F', 'B', 'N', 'V', 'M'],
          ['D', 'H', 'G', 'M', 'R'],
          ['H', 'N', 'M', 'V', 'Z', 'D'],
          ['G', 'N', 'F', 'H']]


print(f"5a result is: {run_5a(crates, instructions)}")
print(f"5b result is: {run_5b(crates, instructions)}")
