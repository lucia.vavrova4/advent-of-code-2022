from functions import input_file, moves

file = input_file('../input/09_input.txt')

head = [0, 0]
tail = [0, 0]
tails = []


for line in file:
    if line[0] == 'R':

        for i in range(int(line[2:])):
            head[1] += 1
            tail = moves(head, tail)
            tails.append(tail.copy())

    if line[0] == 'L':

        for i in range(int(line[2:])):
            head[1] -= 1
            tail = moves(head, tail)
            tails.append(tail.copy())

    if line[0] == 'D':
        for i in range(int(line[2:])):
            head[0] -= 1
            tail = moves(head, tail)
            tails.append(tail.copy())

    if line[0] == 'U':
        for i in range(int(line[2:])):
            head[0] += 1
            tail = moves(head, tail)
            tails.append(tail.copy())

tails.sort()
result = []
for tail in tails:
    if tail not in result:
        result.append(tail)

print(f"9a result is: {len(result)}")


# B


head = [0, 0]
tail = [[0, 0] for _ in range(9)]
tails = []

for line in file:
    if line[0] == 'R':
        for i in range(int(line[2:])):
            head[1] += 1
            tail[0] = moves(head, tail[0])

            for j in range(8):
                tail[j+1] = moves(tail[j], tail[j+1])
                if j == 7:
                    tails.append(tail[j+1].copy())

    if line[0] == 'L':
        for i in range(int(line[2:])):
            head[1] -= 1
            tail[0] = moves(head, tail[0])

            for j in range(8):
                tail[j+1] = moves(tail[j], tail[j+1])
                if j == 7:
                    tails.append(tail[j + 1].copy())

    if line[0] == 'U':
        for i in range(int(line[2:])):
            head[0] -= 1
            tail[0] = moves(head, tail[0])

            for j in range(8):
                tail[j+1] = moves(tail[j], tail[j+1])
                if j == 7:
                    tails.append(tail[8].copy())

    if line[0] == 'D':
        for i in range(int(line[2:])):
            head[0] += 1
            tail[0] = moves(head, tail[0])

            for j in range(8):
                tail[j+1] = moves(tail[j], tail[j+1])
                if j == 7:
                    tails.append(tail[8].copy())

result = []
for item in tails:
    if item not in result:
        result.append(item.copy())

print(f"9b result is: {len(result)}")

